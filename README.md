## RaspBerry Pi 3 - Prometheus Temperature Exporter

### Requirements:
* 1 Raspberry Pi 3
* 1 (at least 1) DS18B20 Temperature Sensor 
* Code Dependencies: python2.7 gunicorn python-rpi.gpio python-flask - [setup.sh](setup.sh) will install these)

#### RaspBerry Pi 3 setup
Ensure that you have ```dtoverlay=w1-gpio``` set in your ```/boot/config.txt``` in Raspbian

#### DS18B20 Setup
This setup is quick, just wire up using a 4.7K ohm pull-up resister between VCC and Data channel.  I used Pi PINS: 1, 7, 9

#### Code installation:
All in one shot:
```curl https://gitlab.com/gitlab_joel/pi_temperature_exporter/raw/master/setup.sh | sudo bash```