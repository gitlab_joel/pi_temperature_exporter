#!/bin/bash

### Host configuration:
apt-get update -q
apt-get install --force-yes -yq git-core python2.7 gunicorn python-rpi.gpio python-flask nginx

## Install Flask-Cors
pip2 install flask-cors


### Application installation and configuration
## first make the /app dir if we dont have it, and remove any existing installation
## of pi_web_temp
if [ ! -e /app/pi_web_temp ]; then
    echo No /app dir found, creating installation directory: /app/pi_web_temp
    mkdir -p /app/pi_web_temp
fi

cd /app/pi_web_temp/
if [ $? -gt 0 ]; then
    echo "CRITICAL: could not switch paths to /app/pi_web_temp!!  Exiting"
    echo $PWD
    exit 1
fi

## now checkout the project to /app
echo 'Pulling down the Temperature Application (pi_web_temp)'
curl -s 'https://gitlab.com/gitlab_joel/pi_temperature_exporter/repository/archive.tar.gz?ref=master' | tar xzf - --strip 1
if [ $? -gt 0 ]; then
    echo "CRITICAL: Could not download Sensor Code from GitLab.com!! Exiting" 
    exit 2
fi

## copy the systemd service manifest into place
echo 'Installing Temperature application to start on boot (pi_web_temp)'
cp /app/pi_web_temp/systemd/* /etc/systemd/system/
if [ $? -gt 0 ]; then
    echo "CIRITCAL: Could not copy the startup script to /etc/systemd/system!!  Exiting"
    exit 3
fi
systemctl daemon-reload

## Nginx configuration
echo 'Installing Nginx configuration'
cp /app/pi_web_temp/nginx/* /etc/nginx/sites-available/
if [ $? -gt 0 ]; then
    echo "CRITICAL: There was an error copying Nginx config files!!  Exiting"
    exit 4
fi
if [ -e /etc/nginx/sites-enabled/pi_web_temp ]; then
    rm /etc/nginx/sites-enabled/pi_web_temp
fi
ln -s /etc/nginx/sites-available/pi_web_temp /etc/nginx/sites-enabled/pi_web_temp

## Remove the default nginx config symlink, if its not a symlink it may have been customized 
## and I dont want to undo what the user has done
if [ -L /etc/nginx/sites-enabled/default ]; then
    rm /etc/nginx/sites-enabled/default
fi

## Start Services
echo 'Starting pi_web_temp and restarting Nginx to pickup the changes'
service pi_web_temp restart
service nginx restart

## Set pi_web_temp service to start on reboot
systemctl enable pi_web_temp

echo 'The web service should now be available on http://your_pi_ip/ and http://your_pi_ip/metrics'

## Setup Pi3 boot conf:
if [ $(grep -qE '^dtoverlay\=w1-gpio' /boot/config.txt) ]; then 
    echo /boot/config.txt is already configured; 
else 
    echo "#######################################"
    echo "#######################################"
    echo "# You MUST ADD 'dtoverlay=w1-gpio'    #"
    echo "# to your /boot/config.txt and reboot #"
    echo "# or your sensors may not be detected #"
    echo "#######################################"
    echo "#######################################"
fi
