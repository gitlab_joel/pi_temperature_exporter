#!/usr/bin/env python2.7

import json
from sys import exit
from os import getpid, listdir, path
from time import time
from datetime import datetime
from subprocess import call
from flask import Flask, jsonify, render_template, Response
from flask_cors import CORS
from collections import defaultdict


# Program variables
CACHE_FILE_PATH = '/tmp'    # An existing directory, with no trailing /
CACHE_EXPIRE_TIME = 60      # how long is the sensor cache good for, in seconds

# A List of drivers we will load
LINUX_DRIVERS = ['w1-gpio', 'w1-therm']

# Path to the 'modprobe' executable
MODPROBE_PATH = '/sbin/modprobe'

# Sensor Pathing information
SENSOR_SYS_PATH = '/sys/bus/w1/devices'
SENSOR_PREFIX = '28-'
SENSOR_SUFFIX = 'w1_slave'

# Flask Related Settings
ALLOWABLE_ORIGINS = ['*']
app = Flask(__name__)
CORS(app, resources={r"/*": {"origins": ALLOWABLE_ORIGINS}})
app.config['DEBUG'] = False


class temp_sensor(object):
    '''Sensor Object, to make things easy later in displaying values and
    refreshing data'''
    def __init__(self):
        self.device_id = None
        self.device_path = None
        self.raw_temp = int
        self.temp_f = int
        self.tem_c = int
        self.poll_time = 0
        self.polled_at = int
        self.name = None

    def setup_sensor(self, device_full_path):
        '''Method which will setup the inital data of the sensor object, it
        also does the first 'refresh_sensor_data()' call to get everything
        plumbed and fail early if there is an issue reading a sensor'''
        self.device_id = device_full_path.split('/')[5]
        self.device_path = '{}/{}'.format(device_full_path, SENSOR_SUFFIX)
        self.name = 'sensor_{}'.format(self.device_id.split('-')[1])
        if self.refresh_sensor_data() is False:
            raise Exception('SENSOR_READ_ERROR',
                            'Couldnt Read [{}]'.format(self.device_path))

    def refresh_sensor_data(self):
        '''This method uses the values that have been set in the object and
        then calls '_get_sensor_data' which tries to open the sensor device and
        sends the raw sensor data downstream to the '_parse_sensor_data()'
        method

        Returns: True/False'''

        poll_start_time = time()
        sensor_retries = 0
        while sensor_retries != 3:
            raw_data = self._get_sensor_data()
            if self._parse_sensor_data(raw_data):
                self.poll_time = (time() - poll_start_time)
                self.polled_at = time()
                return True
            sensor_retries += 1
        return False

    def _get_sensor_data(self):
        '''This method will open the device node and read from it, or throw an
        exception.  It returns the data it reads'''
        try:
            with open(self.device_path, 'r') as sensor:
                return(sensor.readlines())
        except IOError as e:
            print("Error Reading Sensor {}, exiting").format(self.device_id)
            exit(2)

    def _parse_sensor_data(self, raw_data):
        '''This method will try to validate the data received from
        '_get_sensor_data'.  We first look for a 'YES' in the first line which
        indicates that communication on the one-wire-bus was successful, then
        it splits off the last part of the next line, near t=(grab)'''
        if 'YES' in raw_data[0] and raw_data[1]:
            temp_val = raw_data[1].split('=')[1]
            self._parse_temps_from_data(int(temp_val))
            return True
        else:
            return False

    def _parse_temps_from_data(self, raw_temp):
        '''This method transforms the data into usable human values and stores
        them directly in our object'''
        self.temp_c = round(float(raw_temp) / 1000.0, 2)
        self.temp_f = round((self.temp_c * 1.8) + 32.0, 2)


def load_drivers():
    '''This function will load the system drivers needed to plumb the device
    paths and make them available for use'''
    for driver in LINUX_DRIVERS:
        try:
            if app.config['DEBUG'] is True:
                print('Attempting to load Driver [{}]').format(driver)
            call([MODPROBE_PATH, driver])
        except OSError as e:
            print('Could not load Driver, Exiting. Error: {}').format(e)
            exit(1)


def find_valid_sensors():
    '''Function to find valid Dallas one-wire sensors based on a know device
    path(SENSOR_SYS_PATH) and SENSOR_PREFIX and returns a list which contains a
    full path of each device node'''
    devices = listdir(SENSOR_SYS_PATH)
    valid_sensors = list()
    for device in devices:
        if device.startswith(SENSOR_PREFIX):
            valid_sensors.append('{}/{}'.format(SENSOR_SYS_PATH, device))
    return valid_sensors


def setup_sensors(raw_sensor_list):
    '''This function will instantiate the 'temp_sensor' objects, one for each
    passed in through the 'raw_sensor_list'.  During this call, the objects
    will be plumbed and data received'''
    sensor_objects = list()
    for sensor in raw_sensor_list:
        sensor_object = temp_sensor()
        sensor_object.setup_sensor(sensor)
        sensor_objects.append(sensor_object)
    return sensor_objects


def get_sensor_data():
    '''This function is the entrypoint for both application controllers to
    interface with the workload.  First it will try to load linux drivers for
    our sensors (TODO: this may be too wasteful) then it will look for valid
    sensors and finally it calls 'setup_sensors' to create objects for each
    sensor found.  Keep in mind that each sensor has a unique 64-bit address.
    Once device objects are plumbed and returned, we iterate through the sensor
    list and format a quick dictionary 'sensor_data_dict' which is returned to
    the application controllers to be rendered as needed.

    This function will also try to be smart about caching sensor data, by first
    looking to a local filesystem dir for cached objects'''

    # First, Attempt to load the drivers we know work on Raspbian
    load_drivers()

    # Look in the standard device plumbing path, find and return valid sensors
    temp_sensors = find_valid_sensors()
    if len(temp_sensors) < 1:
        print('No valid Temp Sensors were found, exiting')
        exit(1)

    # Setup Sensor Objects
    sensor_objects = setup_sensors(temp_sensors)
    sensor_data_dict = defaultdict(dict)
    for sensor_o in sensor_objects:
        sensor_data_dict[sensor_o.name]['fahrenheit'] = sensor_o.temp_f
        sensor_data_dict[sensor_o.name]['celsius'] = sensor_o.temp_c
        sensor_data_dict[sensor_o.name]['poll_time'] = sensor_o.poll_time
        sensor_data_dict[sensor_o.name]['polled_at'] = sensor_o.polled_at

        if app.config['DEBUG'] is True:
            print('Current Temperature [{}]: {}F').format(sensor_o.device_id,
                                                          sensor_o.temp_f)
            print('- Sensor Poll Time: {}').format(sensor_o.poll_time)
    return sensor_data_dict, sensor_objects
    # TODO: sensor_objects are never used and are ignored upstream, may use
    # them in the future - remove?


def load_cache_file(cache_file):
    '''Simple function to load the contents of a cache file containing a JSON
    object into a dictionary, which is then returned

    Returns: Dict - cache_data'''
    try:
        with open(cache_file, 'r') as cache_fh:
            cache_file_data = cache_fh.read()
            cache_data = json.loads(cache_file_data)
            return cache_data
    except IOError as e:
        print('CRITICAL: Error reading from {}. Error: {}').format(cache_file,
                                                                   e)
    except TypeError as e:
        print('CRITICAL: Error reading Cache file. Error: {}').format(e)
    exit(2)


def check_cache_freshness(cache_file):
    '''This will check the suplied 'cache_file' for freshness by opening the
    file, loading the expected JSON, and comparing the 'polled_at' time of each
    sensor.  If any of the sensor polled_at times - the current time are more
    than the the user specified 'CACHE_EXPIRE_TIME' this function returns False
    otherwise it returns True

    Returns: True/False, cache_data/None'''

    stale_data = 0
    cache_data = None

    if check_cache_exists(cache_file):
        cache_data = load_cache_file(cache_file)
    else:
        cache_data = update_cache_file(cache_file)

    if len(cache_data.keys()) < 1:
        return False

    for sensor in cache_data:
        if time() - cache_data[sensor]['polled_at'] > CACHE_EXPIRE_TIME:
            stale_data += 1

    if stale_data == 0:
        return True, cache_data
    else:
        return False, None


def update_cache_file(cache_file):
    '''This function will open the 'cache_file' and update it with the proper
    json values.

    Returns: True or Exits'''

    data_dict, sensor_objects = get_sensor_data()
    try:
        with open(cache_file, 'w') as cache_fh:
            json.dump(data_dict, cache_fh)

    except IOError as e:
        print('CRITICAL: Error writing to {}. Error: {}').format(cache_file, e)
        exit(2)

    return data_dict


def check_cache_exists(cache_file):
    '''A function to check if our cache file exists, returns True/False.
    really this is just a convienience function'''

    if path.isfile(cache_file):
        return True
    else:
        return False


def get_data():
    '''This function is an interface to the caching system.  Here we will
    detect if recent data has been cached so that we dont have to talk with the
    sensor constantly.  This lends itself better to slower bus speed devices,
    especially in the case of temperature sensors where we dont really
    need/want a different temperature displayed (wind gusts etc) every couple
    of seconds

    Returns: json_data'''
    mypid = getpid()
    cache_file_name = '{}/{}.tmp'.format(CACHE_FILE_PATH, mypid)
    cache_status, cache_data = check_cache_freshness(cache_file_name)
    if cache_status:
        return cache_data
    else:
        return(update_cache_file(cache_file_name))


@app.route("/")
def temperature():
    '''Renders a the data_dict in json format, starting with the sensors 64-bit
    id to make things static when setup in the real world. '''
    # TODO: Eventually it would be nice to have a seperate config file to store
    # ID to human-readable name mappings for the sensors, but this requires
    # some more mulling
    with app.app_context():
        data_dict = get_data()
        return jsonify(data_dict)


@app.route("/hvac", methods = ['GET', 'POST'])
def hvac():
    '''HVAC endpoint to allow the use of generic RESTful switches in HomeAssistant and to use as a generic thermostat'''
    if request.method == 'GET':
        with app.app_context():
            return jsonify(d)

    if request.method == 'POST':
        j = request.get_json()
        # TODO: some validation needs to be done here and some exception handling
        d['is_active'] = j['active']

        if d['is_active'] == "true":
            GPIO.output(pin, 1)
            print("Turning Furnace ON")

        elif d['is_active'] == "false":
            GPIO.output(pin, 0)
            print("Turning Furnace OFF")

        with app.app_context():
            return jsonify(d)


@app.route("/metrics")
def temperature_exporter():
    '''This endpoint is for Prometheus to use, please see the template for an
    explaination of the data output'''
    with app.app_context():
        data_dict = get_data()
        text = render_template('temp_exporter.tmpl', sensor_data=data_dict)
        return Response(text, mimetype='text/plain')

if __name__ == '__main__':
    app.run()
